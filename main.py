from amqpstorm import Connection
from amqpstorm import management
import secrets
import string

# Параметры подключения к RabbitMQ

rabbitmq_addr = 'localhost'
rabbitmq_port = '15672'
rabbitmq_user = 'test'
rabbitmq_password = 'test1234'


rabbitmq_host = f'http://{rabbitmq_addr}:{rabbitmq_port}'
rabbitmq_vhost = '/'

# Список пользователей
users_rw = [
    'rbmq_scrm',
    'rbmq_sendbox',
    'rbmq_messagebird',
    'rbmq_pbx_service'
]

users_r = [
    'rbmq_read'
]


alphabet = string.ascii_letters + string.digits

# Функция для генерации случайного пароля
def generate_random_password():
    return ''.join(secrets.choice(alphabet) for i in range(20))  # Генерируем пароль из 12 символов

# Подключение к RabbitMQ
if __name__ == '__main__':

    API = management.ManagementApi(rabbitmq_host, rabbitmq_user, rabbitmq_password, verify=False)

    try:

        for user in users_rw:
            password = generate_random_password()

            # Создание пользователя
            API.user.create(user, password, tags="policymaker")

            # Установка разрешений для пользователя
            API.user.set_permission(user,
                                      rabbitmq_vhost,
                                      configure_regex='.*',
                                      write_regex='.*',
                                      read_regex='.*')
            print(f"{user} = {password}")

        for user in users_r:
            password = generate_random_password()

            # Создание пользователя
            API.user.create(user, password, tags="monitoring")

            # Установка разрешений для пользователя
            API.user.set_permission(user,
                                      rabbitmq_vhost,
                                      #configure_regex='.*',
                                      #write_regex='.*',
                                      read_regex='.*')
            print(f"{user} = {password}")

    except management.ApiConnectionError as why:
            print('Connection Error: %s' % why)
    except management.ApiError as why:
        print('Failed to create user: %s' % why)


